from setuptools import setup

def readme():
      with open('README.rst') as f:
            return f.read()

setup(name='semantic-forms',
      version='0.1',
      description='Semantic UI support for crispy-forms',
      long_description=readme(),
      classifiers=[
            "Development Status :: 5 - Production/Stable",
            "Environment :: Web Environment",
            "Framework :: Django",
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
            "Programming Language :: JavaScript",
            "Programming Language :: Python :: 2.6",
            "Programming Language :: Python :: 2.7",
            "Programming Language :: Python :: 3.3",
            "Programming Language :: Python :: 3.4",
            "Topic :: Internet :: WWW/HTTP",
            "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
            "Topic :: Software Development :: Libraries :: Python Modules",
      ],
      url='http://github.com/eneepo/semantic-forms',
      author='eneepo',
      license='MIT',
      keywords=['forms', 'django', 'crispy', 'DRY', 'semantic ui'],
      packages=['semantic_ui'],
      install_requires=[
            'django',
            'django-crispy-forms',
      ],
      zip_safe=False)
