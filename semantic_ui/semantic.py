from django.template.loader import render_to_string

from crispy_forms.layout import LayoutObject, Field, Div
from crispy_forms.utils import render_field, flatatt, TEMPLATE_PACK


class InlineField(Field):
    template = "%s/layout/inline_field.html"


class Group(LayoutObject):
    """
    Layout object. It wraps fields in a <div cass="number fields">

    You can set `css_id` for a DOM id and `css_class` for a DOM class. Example::

        Div('form_field_1', 'form_field_2', css_id='div-example', css_class='divs')
    """
    template = "%s/layout/group.html"

    def __init__(self, *fields, **kwargs):
        self.fields = list(fields)

        self.evenly = True
        if hasattr(self, 'evenly') and 'evenly' in kwargs:
            self.evenly = kwargs.pop('evenly')
        if hasattr(self, 'css_class') and 'css_class' in kwargs:
            self.css_class += ' %s' % kwargs.pop('css_class')
        if not hasattr(self, 'css_class'):
            self.css_class = kwargs.pop('css_class', None)

        self.css_id = kwargs.pop('css_id', '')
        self.template = kwargs.pop('template', self.template)
        self.flat_attrs = flatatt(kwargs)
        self.numbers_list = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']
        self.number = self.numbers_list[len(self.fields)] if self.evenly else False
        self.is_child = False

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, **kwargs):
        fields = ''
        for field in self.fields:
            if isinstance(field, Group):
                field.is_child =True

            fields += render_field(
                field, form, form_style, context, template_pack=template_pack, **kwargs
            )

        template = self.template % template_pack
        return render_to_string(template, {'group': self, 'fields': fields})


class Element(LayoutObject):
    """
    Layout object. It wraps fields in a <div cass="number fields">

    You can set `css_id` for a DOM id and `css_class` for a DOM class. Example::

        Div('form_field_1', 'form_field_2', css_id='div-example', css_class='divs')
    """
    template = "%s/layout/element.html"

    def __init__(self, *fields, **kwargs):
        self.fields = list(fields)

        if 'tag' in kwargs:
            self.tag = kwargs.pop('tag')
        else:
            self.tag = 'div'

        if hasattr(self, 'css_class') and 'css_class' in kwargs:
            self.css_class += ' %s' % kwargs.pop('css_class')
        if not hasattr(self, 'css_class'):
            self.css_class = kwargs.pop('css_class', None)

        self.css_id = kwargs.pop('css_id', '')
        self.template = kwargs.pop('template', self.template)
        self.flat_attrs = flatatt(kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK, **kwargs):
        fields = ''
        for field in self.fields:
            fields += render_field(
                field, form, form_style, context, template_pack=template_pack, **kwargs
            )

        template = self.template % template_pack
        return render_to_string(template, {'elem': self, 'fields': fields})


class TR(Element):
    def __init__(self, *fields, **kwargs):
        Element.__init__(*fields, **kwargs)
        self.tag = 'tr'


class TD(Element):
    def __init__(self, *fields, **kwargs):
        Element.__init__(*fields, **kwargs)
        self.tag = 'td'
