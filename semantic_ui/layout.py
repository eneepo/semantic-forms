from crispy_forms.layout import BaseInput


class Submit(BaseInput):
    """
    Used to create a Submit button descriptor for the {% crispy %} template tag::

        submit = Submit('Search the Site', 'search this site')

    .. note:: The first argument is also slugified and turned into the id for the submit button.
    """
    input_type = 'submit'

    def __init__(self, *args, **kwargs):
        self.field_classes = 'ui primary button'
        super(Submit, self).__init__(*args, **kwargs)


class Button(BaseInput):
    """
    Used to create a Submit input descriptor for the {% crispy %} template tag::

        button = Button('Button 1', 'Press Me!')

    .. note:: The first argument is also slugified and turned into the id for the button.
    """
    input_type = 'button'

    def __init__(self, *args, **kwargs):
        self.field_classes = 'ui button'
        super(Button, self).__init__(*args, **kwargs)


class Reset(BaseInput):
    """
    Used to create a Reset button input descriptor for the {% crispy %} template tag::

        reset = Reset('Reset This Form', 'Revert Me!')

    .. note:: The first argument is also slugified and turned into the id for the reset.
    """
    input_type = 'reset'

    def __init__(self, *args, **kwargs):
        self.field_classes = 'ui button'
        super(Reset, self).__init__(*args, **kwargs)
