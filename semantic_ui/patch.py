from django.conf import settings
from crispy_forms import layout
from semantic_ui.layout import Submit, Button, Reset


def patch_all():
    settings.CRISPY_ALLOWED_TEMPLATE_PACKS = ('bootstrap', 'uni_form', 'bootstrap3', 'bootstrap4', 'semantic_ui')
    settings.CRISPY_TEMPLATE_PACK = 'semantic_ui'

    layout.Submit = Submit
    layout.Button = Button
    layout.Reset = Reset
    layout.Column.field_classes = 'column'
