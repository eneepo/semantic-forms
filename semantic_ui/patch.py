from django.conf import settings
import crispy_forms.layout


def patch_all():
    settings.CRISPY_ALLOWED_TEMPLATE_PACKS = ('bootstrap', 'uni_form',
                                              'bootstrap3', 'bootstrap4',
                                              'semantic_ui')
    settings.CRISPY_TEMPLATE_PACK = 'semantic_ui'

    submit_old_init = crispy_forms.layout.Submit.__init__

    def submit_init(self, *args, **kwargs):
        submit_old_init(self, *args, **kwargs)
        self.field_classes = 'ui primary button'
    crispy_forms.layout.Submit.__init__ = submit_init

    button_old_init = crispy_forms.layout.Button.__init__

    def button_init(self, *args, **kwargs):
        button_old_init(self, *args, **kwargs)
        self.field_classes = 'ui button'
    crispy_forms.layout.Button.__init__ = button_init

    reset_old_init = crispy_forms.layout.Reset.__init__

    def reset_init(self, *args, **kwargs):
        reset_old_init(self, *args, **kwargs)
        self.field_classes = 'ui button'
    crispy_forms.layout.Reset.__init__ = reset_init
