from django import template

from semantic_ui.ajax_select import AutoCompleteSelectMultipleField, AutoCompleteSelectMultipleWidget
register = template.Library()


@register.inclusion_tag('semantic/field.html', takes_context=True)
def sui_field(context, item, *args, **kwargs):
    return {
        'item': item,
        'class': kwargs.get('class', ''),
        'field_width': kwargs.get('field_class', ''),
    }
