from django import template

from semantic_ui.ajax_select import AutoCompleteSelectMultipleField, AutoCompleteSelectMultipleWidget
register = template.Library()

@register.filter
def is_autocompleteselectmultiple(field):
    return isinstance(field.field.widget, AutoCompleteSelectMultipleWidget)

